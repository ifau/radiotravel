//
//  PlayerViewController.m
//  RadioTravel
//
//  Created by ifau on 29/07/17.
//  Copyright © 2017 avium. All rights reserved.
//

#import "PlayerViewController.h"
#import "RadioService.h"

@interface PlayerViewController () <RadioServiceDelegate>
@property (nonatomic, strong) RadioService *radioService;

@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
@end

@implementation PlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.radioService = [[RadioService alloc] init];
    self.radioService.delegate = self;
    
    self.playButton.hidden = NO;
    self.stopButton.hidden = YES;
}

- (IBAction)playButtonPressed:(id)sender
{
    [self.radioService play];
}

- (IBAction)stopButtonPressed:(id)sender
{
    [self.radioService pause];
}

#pragma mark - RadioServiceDelegate Methods

- (void)radioServiceDidStartPlaing
{
    self.playButton.hidden = YES;
    self.stopButton.hidden = NO;
}

- (void)radioServiceDidPausePlaing
{
    self.playButton.hidden = NO;
    self.stopButton.hidden = YES;
}

@end
