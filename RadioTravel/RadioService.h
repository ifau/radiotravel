//
//  RadioService.h
//  RadioTravel
//
//  Created by ifau on 29/07/17.
//  Copyright © 2017 avium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol RadioServiceDelegate <NSObject>

- (void)radioServiceDidStartPlaing;
- (void)radioServiceDidPausePlaing;

@end

@interface RadioService : UIResponder

@property (nonatomic, weak) id<RadioServiceDelegate> delegate;

- (void)play;
- (void)pause;

@end
