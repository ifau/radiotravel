//
//  RadioService.m
//  RadioTravel
//
//  Created by ifau on 29/07/17.
//  Copyright © 2017 avium. All rights reserved.
//

#import "RadioService.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

static NSString* const kRadioURLString = @"https://a4.radioheart.ru:9011/liveradio";

@interface RadioService ()
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *streamItem;

@property (nonatomic, strong) NSString *currentArtist;
@property (nonatomic, strong) NSString *currentTitle;
@property (nonatomic, strong) NSData *currentArtwork;
@end

@implementation RadioService

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSURL *streamURL = [NSURL URLWithString:kRadioURLString];
        self.streamItem = [[AVPlayerItem alloc] initWithURL:streamURL];
        self.player = [[AVPlayer alloc] initWithPlayerItem:self.streamItem];
        
        [self.streamItem addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionNew context:NULL];
        [self.player addObserver:self forKeyPath:@"rate" options:NSKeyValueObservingOptionNew context:NULL];
        
        NSError *error = nil;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
        [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:&error];
        [[AVAudioSession sharedInstance] setActive:YES error:&error];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAudioSessionInterruption:) name:AVAudioSessionInterruptionNotification object:AVAudioSession.sharedInstance];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMediaServicesReset) name:AVAudioSessionMediaServicesWereResetNotification object:AVAudioSession.sharedInstance];
        
        MPRemoteCommandCenter.sharedCommandCenter.playCommand.enabled = YES;
        MPRemoteCommandCenter.sharedCommandCenter.pauseCommand.enabled = YES;
        
        [MPRemoteCommandCenter.sharedCommandCenter.playCommand addTarget:self action:@selector(play)];
        [MPRemoteCommandCenter.sharedCommandCenter.pauseCommand addTarget:self action:@selector(pause)];
        
        [self becomeFirstResponder];
    }
    return self;
}

- (void)dealloc
{
    [self resignFirstResponder];
    [self.streamItem removeObserver:self forKeyPath:@"timedMetadata" context:NULL];
    [self.player removeObserver:self forKeyPath:@"rate" context:NULL];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"timedMetadata"] && [object isKindOfClass:[AVPlayerItem class]])
    {
        [self timedMetadataChanged:((AVPlayerItem *)object).timedMetadata];
    }
    else if ([keyPath isEqualToString:@"rate"] && [object isKindOfClass:[AVPlayer class]])
    {
        [self rateChanged:((AVPlayer *)object).rate];
    }
    else
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)handleAudioSessionInterruption:(NSNotification*)notification
{
    NSNumber *interruptionType = [[notification userInfo] objectForKey:AVAudioSessionInterruptionTypeKey];
    NSNumber *interruptionOption = [[notification userInfo] objectForKey:AVAudioSessionInterruptionOptionKey];
    
    switch (interruptionType.unsignedIntegerValue) {
        case AVAudioSessionInterruptionTypeBegan:{
            // • Audio has stopped, already inactive
            // • Change state of UI, etc., to reflect non-playing state
        } break;
        case AVAudioSessionInterruptionTypeEnded:{
            // • Make session active
            // • Update user interface
            // • AVAudioSessionInterruptionOptionShouldResume option
            if (interruptionOption.unsignedIntegerValue == AVAudioSessionInterruptionOptionShouldResume) {
                // Here you should continue playback.
                [self.player play];
            }
        } break;
        default:
            break;
    }
}

- (void)handleMediaServicesReset
{
    // • No userInfo dictionary for this notification
    // • Audio streaming objects are invalidated (zombies)
    // • Handle this notification by fully reconfiguring audio
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    if (event.type == UIEventTypeRemoteControl)
    {
        if (event.subtype == UIEventSubtypeRemoteControlPlay)
            [self play];
        else if (event.subtype == UIEventSubtypeRemoteControlPause
                 || event.subtype == UIEventSubtypeRemoteControlStop)
            [self pause];
    }
}

#pragma mark - Actions

- (void)timedMetadataChanged:(NSArray<AVMetadataItem*>*)timedMetadata
{
    NSString* (^conv2uft8)(NSString *) = ^NSString *(NSString *str)
    {
        NSData *data = [str dataUsingEncoding:NSISOLatin1StringEncoding];
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    };
    
    NSString *firstItemData = timedMetadata.count > 0 ? conv2uft8((NSString *)timedMetadata.firstObject.value) : @"<Без названия>";
//    NSArray *dataParts;
//    
//    if ([firstItemData rangeOfString:@" – "].location != NSNotFound)
//        dataParts = [firstItemData componentsSeparatedByString:@" – "];
//    else
//        dataParts = [firstItemData componentsSeparatedByString:@"–"];
//    
//    if (dataParts.count > 1)
//    {
//        self.currentArtist = dataParts[0];
//        self.currentTitle = dataParts[1];
//    }
//    else
//    {
//        self.currentArtist = @"";
//        self.currentTitle = dataParts[0];
//    }
    
    self.currentArtist = @"Radio Travel";
    self.currentTitle = firstItemData;
    
    [self updateLockScreen];
}

- (void)rateChanged:(float)rate
{
    if (self.isPlaying)
        [self.delegate radioServiceDidStartPlaing];
    else
        [self.delegate radioServiceDidPausePlaing];
}

- (void)play
{
    if (!self.isPlaying)
    {
        [self.player play];
    }
}

- (void)pause
{
    if (self.isPlaying)
    {
        [self.player pause];
    }
}

- (void)updateLockScreen
{
    NSDictionary *nowPlayingInfo = @{MPMediaItemPropertyArtist : self.currentArtist ?: @"",
                                     MPMediaItemPropertyTitle : self.currentTitle ?: @"",
                                     MPNowPlayingInfoPropertyPlaybackRate : self.isPlaying ? @1 : @0};
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nowPlayingInfo;
}

- (BOOL)isPlaying
{
    return ((self.player.rate != 0) && (self.player.error == nil));
}

@end
